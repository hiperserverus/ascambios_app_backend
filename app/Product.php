<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Seller;
use App\Transaction;
use App\ServiceAllow;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\CreatedUpdatedAgoModelTrait;

class Product extends Model
{

    const PRODUCTO_DISPONIBLE = 'disponible';
    const PRODUCTO_NO_DISPONIBLE = 'no disponible';
    const DEFAULT_IMAGE = '2.jpg';

    
    use SoftDeletes, CreatedUpdatedAgoModelTrait;
    
    protected $dates = ['deleted_at'];

    protected $table = 'products';

    protected $fillable = ['name', 'description', 'status', 'image', 'seller_id'];

    protected $hidden = ['pivot'];

    protected $appends = ['created_at'];

public function estaDisponible() {

    return $this->status == Product::PRODUCTO_DISPONIBLE;
}

public function seller() {
    return $this->belongsTo(Seller::class);
}

public function transactions() {
    return $this->hasMany(Transaction::class);
}

public function categories() {
    return $this->belongsToMany(Category::class);
}

public function serviceAllows()  {

    return $this->hasMany(ServiceAllow::class);
}


}
