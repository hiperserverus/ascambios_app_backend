<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Seller;
use App\Runway;
use App\holderAccount;

class PaymentGetaway extends Model
{
    const STATUS_OPTIONS = ['active', 'inactive'];
    const INITIAL_AMOUNT = 0;
    const INITIAL_STATE = 'active';
    
    protected $table = 'payment_getaways';
    protected $fillable = ['payment_identifier'];

    public function user() {

        return $this->belongsTo(User::class);
    }

    public function seller() {

        return $this->belongsTo(Seller::class);
    }

    public function runway() {
        
        return $this->belongsTo(Runway::class);
    }

    public function holderAccount() {

        return $this->belongsTo(HolderAccount::class);
    }
}
