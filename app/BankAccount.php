<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Country;
use App\HolderAccount;
use App\Bank;
use App\Detailpayment;
use App\BankAccountType;
use App\Seller;



class BankAccount extends Model
{

    const STATUS_OPTIONS = ['active', 'inactive'];
    const INITIAL_AMOUNT = 0;
    const INITIAL_STATE = 'active';

    protected $table='bank_accounts';
    protected $fillable=[ 'bank_account_number', 'amount_disponibility', 'status' ];

    public function user() {

        return $this->belongsTo(User::class);
    }

    public function seller() {

        return $this->belongsTo(Seller::class);
    }

    public function country() {

        return $this->belongsTo(Country::class);
    }

    public function bankAccountType() {
        
        return $this->belongsTo(BankAccountType::class);
    }

    public function bank() {
        
        return $this->belongsTo(Bank::class);
    }

    public function holderAccount() {

        return $this->belongsTo(HolderAccount::class);
    }

    public function detailPayment() {
        return $this->hasOne(DetailPayment::class);
    }


}
