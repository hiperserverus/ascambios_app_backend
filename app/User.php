<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\SoftDeletes;

use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Balance;
use App\BankAccount;
use App\HolderAccount;
use App\Deposit;
use App\Whitdraw;
use App\PaymentGetaway;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    const IMAGE_DEFAULT = 'a.png';

    const USUARIO_VERIFICADO = '1';
    const USUARIO_NO_VERIFICADO = '0';

    const ROLE_OPTIONS = ['regular', 'exchanger', 'admin'];

    const USUARIO_REGULAR = 'regular';
    const USUARIO_CAMBIADOR = 'exchanger';
    const USUARIO_ADMINISTRADOR = 'admin';
    

    protected $table = 'users';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'dni', 'name', 'image', 'email', 'phone','ubication_id', 'password', 'verified', 'verification_token', 'role', 'sponsor_id'
    ];

    public function balance() {

        return $this->hasOne(Balance::class);
    }

    public function bankAccounts() {

        return $this->hasMany(BankAccount::class);
    }

    public function paymentGetaways() {

        return $this->hasMany(PaymentGetaway::class);
    }

    public function holderAccounts() {

        return  $this->hasMany(HolderAccount::class);
    }

    public function deposits() {

        return $this->hasMany(Deposit::class);
    }

    public function whitdraws() {

        return $this->hasMany(Whitdraw::class);
    }

    public function setNameAttribute($valor) {
        $this-> attributes['name'] = Str::lower($valor);
    }

    public function getNameAttribute($valor) {
        
        return ucwords($valor); 
    }

    public function setEmailAttribute($valor) {
        $this->attributes['email'] = Str::lower($valor);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verification_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function esVerificado() {

        return $this->verified == User::USUARIO_VERIFICADO;
    }

    public function esAdministrador() {
        return $this->role == User::USUARIO_ADMINISTRADOR;
    }

    public static function generarVerificationtoken() {
        return Str::random(40);
    }
}
