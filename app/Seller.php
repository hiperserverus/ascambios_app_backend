<?php

namespace App;

use App\Product;
use App\Seller;
use App\PaymentGetaway;

use App\BankAccount;

use App\Scopes\SellerScope;


class Seller extends User
{

    protected static function boot() {

        parent::boot();
        static::addGlobalScope(new SellerScope);
    }

    public function products() {
        return $this->hasMany(Product::class);
    }

    public function bankAccounts() {
        return $this->hasMany(BankAccount::class);
    }

    public function rates() {
        return $this->hasMany(Rate::class);
    }

    public function paymentGetaways() {

        return $this->hasMany(PaymentGetaway::class);
    }



}
