<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BankAccount;
use App\User;

class Deposit extends Model
{
    const BALANCE_INITIAL = 0.00;
    const DEFAULT_STATUS = 'pendiente';
    const STATUS_OPTIONS = ['pendiente', 'aprobado', 'rechazado'];
    const DEFAULT_IMAGE = '2.jpg';

    protected $table = 'deposits';
    protected $fillable = ['amount', 'status', 'user_id', 'bank_account_id'];

    public function user() {

        return $this->belongsTo(User::class);
    }

    public function bank_account() {

        return $this->belongsTo(BankAccount::class);
    }

}
