<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\ServiceAllow;

class ServiceType extends Model
{
    protected $table = 'service_types';
    protected $fillable = ['service_name', 'service_description'];

    public function serviceAllow() {

        return $this->hasMany(ServiceAllow::class);
    }
}
