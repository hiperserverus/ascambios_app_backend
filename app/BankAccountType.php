<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccountType extends Model
{
    protected $table = 'bank_account_types';

    protected $fillable = ['bank_account_type_name'];

    public function bankAccount() {

        return $this->hasOne(BankAccount::class);
    }
}
