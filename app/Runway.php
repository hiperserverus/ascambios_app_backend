<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PaymentGetaway;

class Runway extends Model
{
    public function paymentGetaways() {

        return $this->hasmany(PaymentGetaway::class);
    }
}
