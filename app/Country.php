<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Balance;
use App\Bank;
use App\BankAccount;
use App\Moneda;

class Country extends Model
{
    protected $table = 'countrys';

    protected $fillable = ['name', 'code', 'symbol'];

    public function balances() {

        return $this->belongsToMany(Balance::class);
    }

    public function bankAccounts() {

        return $this->hasMany(BankAccount::class);
    }

    public function monedas() {

        return $this->hasMany(Moneda::class);
    }
}
