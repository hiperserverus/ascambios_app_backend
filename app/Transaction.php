<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Buyer;
use App\Product;
use App\RateTransaction;
use App\DetailPayment;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    const DEFAULT_STATUS = 'pendiente';
    const STATUS_OPTIONS = ['pendiente', 'aprobada', 'rechazada'];
    const DEFAULT_IMAGE = '2.jpg';
    
    protected $dates = ['deleted_at'];

    protected $table = 'transactions';
    
    protected $fillable = ['transaction_identifier' ,'image','image2','image2', 'status', 'description'];

    public function buyer() {
        return $this->belongsTo(Buyer::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function rateTransaction() {
        
        return $this->belongsTo(RateTransaction::class);
    }

    public function detailPayment() {
        return $this->belongsTo(DetailPayment::class);
    }
}
