<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class PhotoController extends Controller
{
    public function index($fileName){

        $disk = Storage::disk('images');

      

        try {
        

        $exists = $disk->exists($fileName);

        
        
         if($exists){

            // $path = public_path().'/img/'.$fileName;

            $url = $disk->get($fileName);

     


            
            $img = Image::make($url)->resize(300, 200)->response(); 

             
         }else{
            $path = public_path().'/img/3.jpg';
            
            $img = Image::make($path)->resize(300, 200)->response(); 
         }
         
     

    }catch (\Exception $e) {
        return response()->json([
            'message' => $e->getMessage(),
        ], 400);
    }

    
        return  $img;
    }

    public function create(Request $request) {

        if ( $request->hasFile('images')) {

            //  $imageName = $request->image->store('local');
            $file = $request->file('image');
             $imageExtension = $file->getClientOriginalExtension();
            //  $imageName = 'image-' . '-' . str_random(10) . '.' . $imageExtension;

            // Storage::putFileAs(public_path().'/img/', new File($request->file('image')), $imageName);

            $nombre_imagen = 'image' . "-" . time() . "." .$imageExtension;


       
            // $ruta = public_path() . '/img';

            // Storage::putFileAs($ruta, $file, $nombre_imagen);

             $disk = Storage::disk('image');

             $imageName = $disk->put('',$file);

            // $imageName = $request->image->store('s3');
          

               


        } else {
            return response('3.jpg', 200);
        }

        

        return response($imageName, 200);

    }
}
