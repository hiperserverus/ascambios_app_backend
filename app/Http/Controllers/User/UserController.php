<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Mail;
use App\User;
use App\Mail\UserCreated;

use App\Balance;

use Carbon\Carbon;

class UserController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::all();

        // $users = DB::table('users')
        // ->orderBy('name', 'desc')
        // ->paginate(10);

    //     Carbon::setLocale('es');
    //     $users->each(function($user, $key) {
 
            
    //         $fech = $user->created_at;
    //               $user->created_at = Carbon::parse($fech)->diffForhumans();     
    //    });


    //      return response()->json(['users' => $users]);


         return $this->showAll($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      
        
        $rules = [
            'name' => 'required',
            'email' => 'required | email | unique:users',
             'password' => 'required | min:3 | confirmed'
        ];

        $this->validate($request, $rules);


        // if ($request->has('country')) {
        //     return $this->errorResponse('Debe seleccionar el país', 409);
        // }

        $values = $request->all();

        if($request->has('image')){
            $user->image  = $request->image;
            }else{
            
                $values['image']  = User::IMAGE_DEFAULT;
            }

        $values['dni'] = $request->dni;
        $values['phone'] = $request->phone;
        $values['ubication_id'] = $request->ubication;
        $values['password'] = bcrypt($request->password);
        // $values['verified'] = User::USUARIO_NO_VERIFICADO;
        $values['verified'] = User::USUARIO_VERIFICADO;
        $values['verification_token'] = User::generarVerificationToken();
        $values['role'] = User::USUARIO_REGULAR;
        

        if($request->has('sponsor')){

            $values['sponsor_id'] = $request->sponsor;

            }else{
            
                $values['sponsor_id'] = null;
            }

        $user = User::create($values);

        // return $this->showOne($user, 201);

        //return response()->json(['$user' => $user], 201);
       

        return DB::transaction(function () use ($request, $user) {

            $balance = Balance::create([
                'value' => Balance::BALANCE_INITIAL,
                'user_id' => $user->id,
                'country_id' => $request->country,
            ]);

            return $this->showOne($user, 201);
        });

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // $user = User::findOrFail($id);

        // return response()->json(['user' => $user]);
        return $this->showOne($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // $user = User::findOrFail($id);
        
        $rules = [
            'email' => 'email|unique:users,email,'. $user->id,
            'password' => 'min:6|confirmed',
            'role' => 'in:' . User::USUARIO_ADMINISTRADOR . ',' . User::USUARIO_REGULAR . ',' . User::USUARIO_CAMBIADOR,
        ];

        $this->validate($request, $rules);


        if ($request->has('name')) {
            $user->name = $request->name;
        }




        $user->phone = $request->phone;

  

        if ($request->has('email') && $user->email != $request->email) {
            $user->verified = User::USUARIO_NO_VERIFICADO;
            $user->verification_token = User::generarVerificationToken();
            $user->email = $request->email;
        }

        if ($request->has('password')) {
            $user->password = bcrypt($request->password);
        }

        if ($request->has('role')) {
            if (!$user->esVerificado()) {
                // return response()->json(['error' => 'Unicamente se les puede cambiar su valor de administrador a los usuarios verificados...'
                // , 'status' => 409], 409);
           return $this->errorResponse('Unicamente se les puede cambiar su valor de administrador a los usuarios verificados...', 409);
           
            }

            $user->admin = $request->admin;
        }

        // if (!$user->isDirty()) {
        //     // return response()->json(['error' => 'Debe epecificar almenos un cambio a modificar en la peticion'
        //     // , 'status' => 422], 422);
        //     return $this->errorResponse('Debe epecificar almenos un cambio a modificar en la peticion',422);

        // }

        $user->save();

        // return response()->json(['user' => $user, 'status' => '200'], 200);
        return $this->showOne($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // $user = User::findOrFail($id);

        $user->delete();

        // return response()->json(['user' => $user, 'status' => '200'], 200);
        return $this->showOne($user);
    }

    public function verify($token) {

        $user = User::Where('verification_token', $token)->firstOrFail();
        $user->verified = User::USUARIO_VERIFICADO;
        $user->verification_token = null;
        $user->save();
        return $this->showMessage('La cuenta ha sido verificada');
    }

    public function resend(User $user) {

        if ($user->esVerificado()) {
            return $this->errorResponse('Este usuario ya se encuentra verificado.', 409);
              
        }

        retry(5, function () use ($user) {
            Mail::to($user->email)->send(new UserCreated($user));
            }, 100);

            return $this->showMessage('El correo de verificacion se ha reenviado', 200);

    }
}
