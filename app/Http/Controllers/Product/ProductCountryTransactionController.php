<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

use App\Product;
use App\Country;

class ProductCountryTransactionController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product, Country $country)
    {
       

       $transactions = $product->transactions()->with('rateTransaction.country','detailPayment.bankAccountDestination.holderAccount','detailPayment.bankAccountSeller.holderAccount'
        ,'detailPayment.bankAccountDestination.bank', 'detailPayment.bankAccountSeller.bank','detailPayment.bankAccountDestination.bankAccountType', 'detailPayment.bankAccountSeller.bankAccountType')
        ->get()
        ->where('rateTransaction.country.id',$country->id);
   

    

        return $this->showAll($transactions);
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
