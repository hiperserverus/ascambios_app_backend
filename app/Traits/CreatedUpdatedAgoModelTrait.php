<?php

namespace App\Traits;
use Carbon\Carbon;

trait CreatedUpdatedAgoModelTrait {




public function getCreatedAtAttribute() {

    Carbon::setLocale('es');

    return Carbon::parse($this->attributes['created_at'])->diffForhumans();


}

public function getUpdatedAgoAttribute() {

    if ($this->updated_at) {
        return $this->updated_at->diffForHumans();
    }
    return '';
}

public function getDeletedAgoAttribute() {

    if ($this->deleted_at) {
        return $this->deleted_at->diffForHumans();
    }
    return '';
}


}