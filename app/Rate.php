<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Seller;
use App\Moneda;
use App\ServiceAllow;

class Rate extends Model
{
    const RATE_INITIAL = 0.00;

    protected $table = 'rates';

    protected $fillable = ['quantity', 'min_quantity', 'buy_rate', 'sell_rate'];

    public function serviceAllow() {

        return $this->belongsTo(ServiceAllow::class);
    }
}
