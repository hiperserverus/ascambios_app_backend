<?php

namespace App;

use App\Country;
use App\BankAccount;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table='banks';
    protected $fillable=['bank_name' ];

    public function bankAccounts() {

        return $this->hasmany(BankAccount::class);
    }


}
