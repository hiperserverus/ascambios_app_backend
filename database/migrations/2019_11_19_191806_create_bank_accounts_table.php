<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\BankAccount;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('bank_account_type_id');
            $table->string('bank_account_number')->unique();
            $table->float('amount_disponibility', 8, 2)->default(BankAccount::INITIAL_AMOUNT);

            $table->unsignedBigInteger('bank_id');
            $table->unsignedBigInteger('holder_account_id');

            $table->enum('status',BankAccount::STATUS_OPTIONS)->default(BankAccount::INITIAL_STATE);

            $table->unsignedBigInteger('seller_id');
            $table->unsignedBigInteger('country_id');


            
            

            $table->timestamps();

            $table->foreign('bank_account_type_id')->references('id')->on('bank_account_types');
            $table->foreign('bank_id')->references('id')->on('banks');
            $table->foreign('holder_account_id')->references('id')->on('holder_accounts');
            $table->foreign('seller_id')->references('id')->on('users');
            $table->foreign('country_id')->references('id')->on('countrys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
