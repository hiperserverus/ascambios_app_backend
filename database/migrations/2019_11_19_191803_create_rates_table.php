<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Rate;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('quantity', 20, 8)->default(Rate::RATE_INITIAL);
            $table->float('min_quantity', 20, 8)->default(Rate::RATE_INITIAL);
            $table->float('buy_rate', 8, 2)->default(Rate::RATE_INITIAL);
            $table->float('sell_rate', 8, 2)->default(Rate::RATE_INITIAL);
            $table->unsignedBigInteger('moneda_emitter_id');
            $table->unsignedBigInteger('moneda_recepter_id');
            $table->unsignedBigInteger('service_allow_id');

            $table->timestamps();


            $table->foreign('moneda_emitter_id')->references('id')->on('monedas');
            $table->foreign('moneda_recepter_id')->references('id')->on('monedas');
            $table->foreign('service_allow_id')->references('id')->on('service_allows');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rates');
    }
}
