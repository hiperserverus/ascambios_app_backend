<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Whitdraw;

class CreateWhitdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whitdraws', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('amount', 20, 8);
            $table->enum('status',Whitdraw::STATUS_OPTIONS)->default(Whitdraw::DEFAULT_STATUS);

            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('bank_account_id');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whitdraws');
    }
}
