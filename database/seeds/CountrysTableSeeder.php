<?php

use Illuminate\Database\Seeder;

use App\Country;
use App\Moneda;

class CountrysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countrys = [
            'ARGENTINA',
            'BRASIL',
            'BOLIVIA',
            'COLOMBIA',
            'CHILE',
            'ECUADOR',
            'URUGUAY',
            'PARAGUAY',
            'VENEZUELA',
            'GUATEMALA',
            'SALVADOR',
            'ESTADOS UNIDOS',
            'MÉXICO',
            'PERÚ',
            'CUBA'
        ];

        $codes = [
            '032',
            '986',
            '984',
            '170',
            '152',
            '840',
            '858',
            '600',
            '928',
            '320',
            '222',
            '840',
            '484',
            '604',
            '192'
        ];

        $symbols = [
            'ARS',
            'BRL',
            'BOB',
            'COP',
            'CLP',
            'EC',
            'UYU',
            'PYG',
            'VES',
            'GTQ',
            'SVC',
            'USD',
            'MXN',
            'PEN',
            'CUP'
        ];

        $phones = [
            '+54',
            '+55',
            '+591',
            '+57',
            '+56',
            '+593',
            '+598',
            '+595',
            '+58',
            '+502',
            '+503',
            '+1',
            '+52',
            '+51',
            '+53'
        ];

        $monedas = [
            'PESO ARGENTINO (ARS)',
            'REAL BRASILERO (BRL)',
            'BOLIVIANO (BOB)',
            'PESO COLOMBIANO (COP)',
            'PESO CHILENO (CLP)',
            'DOLAR ECUADOR (EC)',
            'PESO URUGUAYO (UYU)',
            'GUARANI PARAGUAYO (PYG)',
            'BOLIVAR SOBERANO (VES)',
            'QUETZAL GUATEMALTECO (GTQ)',
            'DOLAR EL SALVADOR (SV)',
            'DOLARES (EEUU)',
            'PESO MEXICANO (MEX)',
            'SOL PERUANO (PEN)',
            'PESO CUBANO (CUP)'
        ];

        $monedasUSA = [
            'PAYPAL',
            'SKRILL',
            'PAYEER',
            'PAYONNER',
            'NETELLER',
            'BITCOIN'

        ];

        for ($i=0; $i < count($countrys); $i++) {

            $country = new Country();
            $country->name = utf8_encode(ucwords(strtolower($countrys[$i])));
            $country->code = utf8_encode(ucwords(strtolower($codes[$i])));
            $country->symbol = utf8_encode(ucwords(strtolower($symbols[$i])));
            $country->phone_code = utf8_encode(ucwords(strtolower($phones[$i])));
       
         
            $country->save();
           $moneda = new Moneda();
           $moneda->name = utf8_encode(ucwords(strtolower($monedas[$i])));
           $moneda->country_id = $country->id;
           $moneda->save();

           

             if ( $countrys[$i] == 'ESTADOS UNIDOS') {

                for ($j=0; $j < count($monedasUSA); $j++) {

                    $moneda = new Moneda();
                    $moneda->name = utf8_encode(ucwords(strtolower($monedasUSA[$j])));
                    $moneda->country_id = $country->id;
                    $moneda->save();

                }
            }

        }
    }
}
