<?php

use Illuminate\Database\Seeder;

use App\ServiceType;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            'ENVIO DE REMESAS',
            'PAYPAL (COMPRA/VENTA)',
            'SKRILL (COMPRA/VENTA)',
            'NETELLER (COMPRA/VENTA)',
            'PAYEER (COMPRA/VENTA)',
            'PAYONNER (COMPRA/VENTA)',
            'BITCOIN (COMPRA/VENTA)'          
        ];

        for ($i=0; $i < count($services); $i++) {

            $service = new ServiceType();
            $service->service_name = utf8_encode(ucwords(strtolower($services[$i])));
       

            $service->save();
        }
    }
}
