<?php

use Illuminate\Database\Seeder;

use App\PaymentMethod;

class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $methods = [
            'TRANSFERENCIA BANCARIA',
            'PAYPAL',
            'PAYEER',
            'PAYONNER',
            'NETELLER',
            'SKRILL',
            'YAPE',
            'BITCOIN'          
        ];

        for ($i=0; $i < count($methods); $i++) {

            $method = new PaymentMethod();
            $method->payment_method_name = utf8_encode(ucwords(strtolower($methods[$i])));
       

            $method->save();
        }
    }
}
